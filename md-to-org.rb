def combinar_y_convertir_a_org(carpeta, destino)
  # Lista de archivos Markdown en la carpeta especificada
  archivos_md = Dir.glob(File.join(carpeta, '**', '*.md'))
  puts archivos_md.inspect
  raise "Se necesitan al menos 2 archivos Markdown para combinar." if archivos_md.size < 2

  # Verificar si la carpeta de destino está vacía
  if !Dir.empty?(destino)
    puts "Advertencia: La carpeta de destino '#{destino}' no está vacía. Los archivos existentes se omitirán en la combinación."
    archivos_md.reject! { |archivo| File.exist?(File.join(destino, File.basename(archivo))) }
  end

  if archivos_md.empty?
    puts "No hay nuevos archivos Markdown para combinar."
    return
  end

  system("pandoc #{archivos_md.join(' ')} -o #{File.join(destino, 'todos_cheatsheets.md')}")
  puts "Se combinaron #{archivos_md.size} archivos Markdown."

  system("pandoc -f markdown -t org -o #{File.join(destino, 'todos_cheatsheets.org')} #{File.join(destino, 'todos_cheatsheets.md')}")
  puts "¡Conversión completada! Archivo todos_cheatsheets.md y todos_cheatsheets.org creados en la carpeta '#{destino}'."
rescue StandardError => e
  puts "Ocurrió un error durante la conversión: #{e.message}"
end

# Carpeta que contiene los archivos Markdown (argumento 1)
carpeta_cheatsheets = ARGV[0]

# Carpeta de destino para los archivos combinados (argumento 2)
carpeta_destino = ARGV[1]

# Verificar si se proporcionaron los argumentos necesarios
if carpeta_cheatsheets.nil? || carpeta_destino.nil?
  puts "Uso: ruby convertir_a_org.rb <carpeta_origen> <carpeta_destino>"
else
  # Ejecutar la combinación y conversión
  combinar_y_convertir_a_org(carpeta_cheatsheets, carpeta_destino)
end
